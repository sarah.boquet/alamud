# -*- coding: utf-8 -*-
# Copyright (C) 2014 Denys Duchier, IUT d'Orléans
#==============================================================================

from .action import Action2
from mud.events import LightEvent

class LightAction(Action2):
    EVENT = LightEvent
    ACTION = "light2"
    RESOLVE_OBJECT = "resolve_for_use"