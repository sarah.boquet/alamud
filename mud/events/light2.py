# -*- coding: utf-8 -*-
# Copyright (C) 2014 Denys Duchier, IUT d'Orléans
#==============================================================================

from .event import Event2

class LightEvent(Event2):
    NAME = "light2"

    def perform(self):
        if not self.object.has_prop("lightable"):
            self.fail()
            return self.inform("light2.failed")
        self.inform("light2")