from .event import Event2

class KillEvent(Event2):
	NAME = "kill"

	def perform(self):
		if not self.object.has_prop("killable"):
			self.fail()
			return self.inform("kill.failed")
		self.inform("kill") 